#------------------------------------
#
#
#
#
#

oo::class create Eventos \
{
    variable nome
    constructor {}\
    {
        global imagens
        set nome ".desktop.eventos[clock click]"
        ttk::frame $nome
        pack [ttk::frame $nome.lt] -side left -fill y
        pack [ttk::frame $nome.lt.tb] -side top -fill x
        pack [ttk::labelframe $nome.lt.panel -text "Eventos:" -relief ridge -width 200] -side top -fill both -expand 1
        pack [ttk::scrollbar $nome.lt.panel.sc -orient vertical -command [list $nome.lt.panel.lista yview]] -fill y -side right
        pack [ttk::treeview $nome.lt.panel.lista -yscrollcommand [list $nome.lt.panel.sc set] -show headings -columns [list descricao_evento total] -displaycolumns [ list descricao_evento total]] -side top -fill both -expand 1
        $nome.lt.panel.lista column descricao_evento -width 120
        $nome.lt.panel.lista column total -width 80
        $nome.lt.panel.lista heading descricao_evento -text "Evento"
        $nome.lt.panel.lista heading total -text "Tot. gasto"
        pack [ttk::button $nome.lt.tb.btFechar -image [dict get $imagens "fechar_p"] -command [list destroy $nome] ] -side left
        pack [ttk::label $nome.lt.tb.sep1 -text "|" -width 2] -side left
        pack [ttk::button $nome.lt.tb.addbt -image [dict get $imagens "mais_p"] -command [list [self] add_evento]] -side left 
        pack [ttk::button $nome.lt.tb.editbt -image [dict get $imagens "editar_p"] -command [list [self] editar_evento]] -side left
        pack [ttk::label $nome.lt.tb.sep2 -text "|"] -side left
        pack [ttk::button $nome.lt.tb.delbt -image [dict get $imagens "apagar_p"] -command [list [self] apagar_evento]] -side left
        pack [ttk::notebook $nome.desktop] -side right -fill both -expand 1
        .desktop add $nome -text "Gastos por eventos"
        .desktop select $nome
        [self] atualiza_lista
    }

    method apagar_evento {}\
    {
       set item [$nome.lt.panel.lista item [$nome.lt.panel.lista selection] -values]
       if {[llength $item]<=0} return
       if {[tk_messageBox -message "Apagar o evento [lindex $item 0]?" -type yesno -icon question]==yes}\
       {
           db eval "DELETE FROM eventos WHERE descricao_evento='[lindex $item 0]'"
           [self] atualiza_lista
       }
    }

    method editar_evento {}\
    {
        set item [$nome.lt.panel.lista item [$nome.lt.panel.lista selection] -values]
        if {[llength $item]<=0} return
        Evento new [self] [ lindex $item 0]
    }

    method add_evento {}\
     {
         set descricao [input_box "Qual a descrição do evento:"]
         #teste se a descricao ja existe na tabela de eventos, caso exista cancela
         if {$descricao==""} return
         if {[db eval "SELECT COALESCE((SELECT 1 FROM eventos WHERE descricao LIKE '$descricao'),0) as resultado"]==1} \
         {
             tk_messageBox -message "Evento já existente" -icon error
             return
         } else {
            Evento new [self] $descricao
        }
    }

    method atualiza_lista {} \
    {
        $nome.lt.panel.lista delete [$nome.lt.panel.lista children {}]
        db eval "SELECT descricao_evento FROM eventos GROUP BY (descricao_evento)" \
        {
            set total [format "%.2f" [db eval "SELECT sum(valor) FROM eventos WHERE descricao_evento='$descricao_evento'"]]
            $nome.lt.panel.lista insert "" end -values [list $descricao_evento  $total]
        }
    }
 
    method get_nome {} { return $nome}
    
}

oo::class create Evento {
    variable nome
    variable parent
    variable descricao

    constructor {prt desc} \
    {
        global imagens
        set parent $prt
        set descricao $desc
        set nome [ttk::frame "[$parent get_nome].desktop.evento$descricao[clock click]"]
        pack [ttk::frame $nome.tb -relief ridge -border 2] -side top -fill x
        pack [ttk::button $nome.tb.b1 -image [dict get $imagens "fechar_p"] -command [list [self] destroy]] -side left
        pack [ttk::label $nome.tb.sep1 -text "|" -width 2] -side left
        pack [ttk::button $nome.tb.b2 -image [dict get $imagens "mais_p"] -command [list [self] add] ] -side left
        pack [ttk::button $nome.tb.b3 -image [dict get $imagens "editar_p"] -command [list [self] editar] ] -side left
        pack [ttk::label $nome.tb.sep2 -text "|" -width 2] -side left
        pack [ttk::button $nome.tb.b4 -image [dict get $imagens "apagar_p"] -command [list [self] apagar]] -side left
        pack [ttk::scrollbar $nome.scv -orient vertical -command [list $nome.lista yview]] -side right -fill y
        pack [ttk::scrollbar $nome.sch -orient horizontal -command [list $nome.lista xview]] -side bottom -fill x
        pack [ttk::treeview $nome.lista -columns [list codigo data descricao valor] -displaycolumns [ list data descricao valor]\
            -yscrollcommand [list $nome.scv set] -xscrollcommand [list $nome.sch set] -show headings] -side top -fill both -expand 1
        $nome.lista heading data -text "Data:"
        $nome.lista heading descricao -text "Descrição:"
        $nome.lista heading valor -text "Valor:"
        $nome.lista column "data" -width 50
        $nome.lista column "valor" -width 50
        [$parent get_nome].desktop add $nome -text "Evento: $descricao"
        [$parent get_nome].desktop select $nome
        [self] atualiza_lista
    }

    method apagar {}\
    {
        set item [$nome.lista item [$nome.lista selection] -values]
        if {[lindex $item]<=0} return
        set msg "Apagar o lançamento \n [lindex $item 1] - [lindex $item 2] Valor [lindex $item 3]"
        if {[tk_messageBox -message $msg -type yesno -icon question]=="yes"}\
        {
            db eval "DELETE FROM eventos WHERE codigo='[lindex $item 0]'"
        }
        [self] atualiza_lista
        $parent atualiza_lista
    }

    method editar {}\
    {
        set item [$nome.lista item [$nome.lista selection] -values]
        if {[lindex $item]<=0} return
        set dlg [GastoDLG new ".dlgEventosGastosEditar"]
        $dlg set [list [lindex $item 1] [lindex $item 2] [lindex $item 3]]
        if [$dlg run] \
        {
            set ret [$dlg get]
            db eval "UPDATE eventos SET data='[lindex $ret 0]',descricao='[lindex $ret 1]',valor='[lindex $ret 2]' WHERE codigo='[lindex $item 0]'"
            [self] atualiza_lista
            $parent atualiza_lista
        }
        $dlg destroy
    }
    
    method add {} \
    {
        set dlg [GastoDLG new ".dlgEventoGastos[clock click]"]
        if [$dlg run]\
        {
            set valores [$dlg get]
            db eval "INSERT INTO eventos(descricao_evento,data,descricao,valor) SELECT \
            '$descricao','[lindex $valores 0]','[lindex $valores 1]','[lindex $valores 2]' WHERE NOT EXISTS\
            (SELECT 1 FROM eventos WHERE descricao_evento='$descricao' AND descricao='[lindex $valores 1]' AND data='[lindex $valores 0]' AND valor='[lindex $valores 1]')"
        }
        $dlg destroy
        [self] atualiza_lista
        $parent atualiza_lista
    }

    method atualiza_lista {}\
    {
        set total 0
        $nome.lista delete [$nome.lista children {}]
        db eval "SELECT codigo,data,descricao as desc,valor
         FROM eventos WHERE descricao_evento='$descricao' ORDER BY data" \
        {
            $nome.lista insert "" end -values [ list $codigo $data $desc [format "%.2f" $valor]]
            set total [expr $total + $valor]
        }
        $nome.lista insert "" end -values [list "" "" "Total" [format "%.2f" $total]]
    }
    
    destructor \
    {
        destroy $nome
    }
}
