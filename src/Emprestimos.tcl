#
#
#
oo::class create Emprestimos {
	variable nome
	global imagens 
	constructor {} \
	{
	global imagens
	set nome [ttk::frame ".desktop.frEmprestimos[clock clicks]"]
	.desktop add $nome -text "Emprestimos"
	.desktop select $nome
	pack [ttk::frame $nome.toolbar -relief ridge -border 3px ] -side top -fill x
	pack [ttk::button $nome.toolbar.bt1 -image [dict get $imagens "fechar_p"] -command [list [self] destroy] ] -side left
	pack [ttk::frame $nome.toolbar.sep1 -relief flat -border 3px -width 10px] -side left -fill y
	pack [ttk::button $nome.toolbar.bt2 -image [dict get $imagens "mais_p"] -command [list [self] novo] ] -side left
	pack [ttk::button $nome.toolbar.bt3 -image [dict get $imagens "editar_p"] -command [list [self] editar] ] -side left
	pack [ttk::frame $nome.toolbar.sep2 -relief raised -border 3px -width 10px] -side left
	pack [ttk::button $nome.toolbar.bt4 -image [dict get $imagens "apagar_p"] -command [list [self] apagar] ] -side left
	pack [ttk::scrollbar $nome.scv -orient vertical] -side right -fill y
	pack [ttk::scrollbar $nome.sch -orient horizontal] -side bottom -fill x
	pack [ttk::treeview $nome.lista -yscrollcommand [list $nome.scv set] -xscrollcommand [list $nome.sch set] \
	-columns [list "codigo" "descricao" "data" "datap" "nparcelas" "valorp" "valor" "taxajuros" "valorjuros"] \
	-displaycolumns [list  "descricao" "data" "datap" "nparcelas" "valorp" "valor" "taxajuros" "valorjuros"] -show headings]  -side top -fill both -expand 1
	$nome.sch configure -command [list $nome.lista xview]
	$nome.lista heading "descricao" -text "Descrição"
	$nome.lista heading "data" -text "data"
	$nome.lista heading "datap" -text "data 1ª parc."
	$nome.lista heading "nparcelas" -text "Nº parc."
	$nome.lista heading  "valorp" -text "val. parcela"
	$nome.lista heading  "valor" -text "val. emprest."
	$nome.lista heading  "taxajuros" -text "taxa juros"
	$nome.lista heading  "valorjuros" -text "valor juros"
	$nome.lista column "descricao" -width 200
	$nome.lista column "data" -width 100
	$nome.lista column "datap" -width 100
	$nome.lista column "valor" -width 100
	$nome.lista column "nparcelas" -width 50
	$nome.lista column "valorp" -width 100
	$nome.lista column "taxajuros" -width 80
	$nome.lista column "valorjuros" -width 80
	[self] atualiza_lista
	}

	method novo {}\
	{
		set dlg [DLGEmprestimos new "."]
		if [$dlg run]\
		{
			set r [$dlg get]
			set sql_cmd " INSERT INTO emprestimos(descricao,data,pparcela,nparcela,valorparcela,valor) SELECT '[dict get $r {descricao}]',\
			'[dict get $r {data}]',\
			'[dict get $r {pparcela}]',\
			'[dict get $r {quantidadeparcelas}]',\
			'[dict get $r {valorparcela}]',\
			'[dict get $r {valor}]'  WHERE NOT EXISTS(SELECT 1 FROM emprestimos WHERE descricao='[dict get $r {descricao}]' AND data='[dict get $r {data}]' AND valor='[dict get $r {valor}]')"
			db eval $sql_cmd
			[self] atualiza_lista
		}
		$dlg destroy 
	}

	method apagar {}\
	{
		set dados [$nome.lista item [$nome.lista selection] -value]
		if {[llength $dados]<=0} return
		set txt "Deseja apagar os dados do emprestimo:\n[lindex $dados 1] \nValor:[format {%2f} [lindex $dados 6]]"
		if [tk_messageBox -message $txt -type yesno -icon question]=="yes"\
		{
			db eval "DELETE FROM emprestimos WHERE codigo='[lindex $dados 0]'"
			[self] atualiza_lista
		}
	}

	method editar {}\
	{
		set dados [$nome.lista item [$nome.lista selection] -value]
		if {[llength $dados]<=0} return
		set dlg [DLGEmprestimos new "."]
		$dlg set [dict create "descricao" [lindex $dados 1] "data" [lindex $dados 2]  "pparcela" [lindex $dados 3] "quantidadeparcelas" [lindex $dados 4]  "valorparcela"  [lindex $dados 5]  "valor" [lindex $dados 6] ]
		if [$dlg run]\
		{
			set valores [$dlg get]
			db eval "UPDATE emprestimos SET descricao='[dict get $valores {descricao}]',\
			data='[dict get $valores {data}]',\
			pparcela='[dict get $valores {pparcela}]',\
			nparcela='[dict get $valores {quantidadeparcelas}]',\
			valorparcela='[dict get $valores {valorparcela}]',\
			valor='[dict get $valores {valor}]'\
			WHERE codigo='[lindex $dados 0]'"
			[self] atualiza_lista
		}
	}

	method atualiza_lista {}\
	{
		$nome.lista delete [$nome.lista children {}]
		db eval "SELECT codigo,descricao,data,pparcela,nparcela,valorparcela,valor FROM emprestimos" {
			set valorjuros [format "%.2f" [expr ($valorparcela * $nparcela)-$valor]]
			set taxajuros [format "%.4f" [mafin::I $valor $valorparcela $nparcela]]
			$nome.lista insert "" end -values [list $codigo $descricao $data $pparcela $nparcela [format "%.2f" $valorparcela] [format "%.2f" $valor] $taxajuros $valorjuros]
		}
	}

	destructor \
	{
		destroy $nome
	}
}
