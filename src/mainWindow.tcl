#
#
#

::oo::class create MainWindow {

	constructor {}\
	{
		global imagens
        global appPath
		wm title . "Orçamento"
		wm geometry . "800x600+100+100"
		[self] make_menu
		#pack [ttk::frame .toolbar -relief ridge -border 2] -side top -fill x
		#pack [ttk::button .toolbar.bt1 -image [dict get $imagens "sair"] -command exit] -side left
		#pack [ttk::frame .toolbar.sep1 -border 5 -relief raised -width 4px] -side left -fill y
        #pack [ttk::button .toolbar.btcalc -image [dict get $imagens "calculadora"] -command {Calculadora new} ] -side left
		#pack [ttk::button .toolbar.bt2 -image [dict get $imagens "contas"] -command [list [self] contas]] -side left
		#pack [ttk::button .toolbar.bt3 -image [dict get $imagens "gastos"] -command [list [self] gastos]] -side left
		#pack [ttk::button .toolbar.bt4 -image [dict get $imagens "parcelados"] -command [list [self] parcelados]] -side left
		#pack [ttk::button .toolbar.bt5 -image [dict get $imagens "emprestimos" ] -command [list [self] emprestimos] ] -side left
	 	#pack [ttk::button .toolbar.bt6 -image [dict get $imagens "cartao"] -command  [list [self] faturas ] ] -side left
		pack [ttk::notebook .desktop] -side top -fill both -expand 1
        #barra de status
        pack [ttk::frame .status -relief ridge -border 2] -side bottom -fill x
        pack [ttk::label .status.stat1 -text "informacoes db"] -side left -fill x -expand 1
        pack [ttk::label .status.relogio -text "AAAA-MM-DD" -relief ridge -border 1] -side right 
        db eval "SELECT sqlite_version() as versao" { .status.stat1 configure -text "Path $appPath Vers�o sqlite $versao"}
        #
		.desktop add [ttk::frame .desktop.ti ]
		pack [ttk::label .desktop.ti.lb -image [dict get $imagens "fundo"] -width 600 ]
        [self] relogio
	}

	method make_menu {}\
	{
		menu .menu
		#Menu dos modulos
		menu .menu.m1 -tearoff 0
		.menu.m1 add command -command [list [self] contas] -label "Contas"
		.menu.m1 add command -command [list [self] gastos] -label "Gastos"
		.menu.m1 add command -command [list [self] parcelados] -label "Compras parceladas"
		.menu.m1 add command -command [list [self] cartoes] -label "Cartões de crédito"
		.menu.m1 add command -command [list [self] emprestimos] -label "Emprestimos"
		.menu.m1 add command -command [list [self] eventos] -label "Gastos em eventos"
		.menu.m1 add separator
		.menu.m1 add command -command exit -label "Sair"
		#Menu dos utilitarios
		menu .m2 -tearoff 0
		.m2 add command -command {Calculadora new} -label "Calculadora"
		#--------------------
		.menu add cascade -menu .menu.m1 -label "Orçamento"
		.menu add cascade -menu .m2 -label "Utilitarios"
		. configure -menu .menu
	}

	method cartoes {} { Faturas new }
	method emprestimos {} { Emprestimos new }
	method parcelados {} { Parcelados new }
	method gastos {} { Gastos new }
	method contas {} { Contas new }
    method faturas {} { Faturas new }
    method eventos {} { Eventos new }
    
    method relogio {} \
    {
        .status.relogio configure -text [clock format [clock seconds] -format "%Y-%m-%d - %H:%M:%S"]
        after 1000 [list [self] relogio]
    }
}
