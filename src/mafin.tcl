
namespace eval mafin {
#legenda
# pv->Valor inicial
# i->Taxa de juros
# n-> Quantidade de prestacoes
# pmt -> valor das prestacoes mensais

proc PMT {pv i n} {
    return [expr {$pv * $i / (1 - (1 + $i) ** -$n)}]
}

proc I {pv pmt n} {
    set i_min 0.0001   ;# Valor mínimo de taxa de juros
    set i_max 1.0      ;# Valor máximo de taxa de juros
    set epsilon 0.00001 ;# Precisão desejada

    # Repetir até a precisão desejada ser atingida
    while {([expr {$i_max - $i_min}] > $epsilon)} {
            set i_mid [expr {($i_min + $i_max) / 2}]
            set prestacao [PMT $pv $i_mid $n]
            # Ajuste do intervalo com base na comparação entre as prestações calculadas
            if {[expr {$prestacao < $pmt}]} {
                set i_min $i_mid
            } else {
                set i_max $i_mid
            }
        }
        # Retorna a taxa de juros encontrada
        return [expr [expr {($i_min + $i_max) / 2}] *100]
    }

}
