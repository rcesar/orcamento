#
#
#
#
oo::class create Parcelados {

    variable nome

    constructor {}\
    {
        global imagens
        set nome [ttk::frame ".desktop.frParcelados[clock clicks]"]
        .desktop add $nome -text "Parcelamentos"
        .desktop select $nome
        pack [ttk::frame $nome.tb -relief ridge -border 2px] -side top -fill x
        pack [ttk::button $nome.tb.bt1 -command [list [self] destroy] -image [dict get $imagens "fechar_p"]] -side left
        pack [ttk::frame $nome.tb.separator -border 2px -relief raised -width 2px ] -side left -fill y
        pack [ttk::button $nome.tb.bt2 -command [list [self] novo] -image [dict get $imagens "mais_p"]] -side left
        pack [ttk::button $nome.tb.bt3 -command [list [self] editar] -image [dict get $imagens "editar_p"]] -side left
        pack [ttk::frame $nome.tb.separator2 -border 2px -relief raised -width 2px ] -side left -fill y        
        pack [ttk::button $nome.tb.bt4 -command [list [self] apagar] -image [dict get $imagens "apagar_p"]] -side left 
        pack [ttk::scrollbar $nome.scwv -orient vertical] -side right -fill  y
        pack [ttk::scrollbar $nome.scwh -orient horizontal] -side bottom -fill x
        pack [ttk::treeview $nome.lista -show headings -columns [list "codigo" "descricao" "data" "pparcela" "numparcelas"  "parcelaspagas" "vparcela"  "saldo" "total"]] -side right -fill both -expand 1
        $nome.lista configure -displaycolumns [list "descricao" "data" "pparcela" "numparcelas" "parcelaspagas" "vparcela" "saldo" "total"]
        $nome.lista column "descricao" -width 150
        $nome.lista column "data" -width 80
        $nome.lista column "pparcela" -width 100
        $nome.lista column "parcelaspagas" -width 80
        $nome.lista column "numparcelas" -width 100
        $nome.lista column "vparcela" -width 80
        $nome.lista column "saldo" -width 80
        $nome.lista column "total" -width 100
        $nome.lista heading "codigo" -text ""
        $nome.lista heading "descricao" -text "Descrição"
        $nome.lista heading "data" -text "Data"
        $nome.lista heading "pparcela" -text "Prim. parcela"
        $nome.lista heading "numparcelas" -text "Nº parc."
        $nome.lista heading "parcelaspagas" -text "Nº Parc. pagas"
        $nome.lista heading "vparcela" -text "Val. Parc."
        $nome.lista heading "saldo" -text "Saldo"
        $nome.lista heading "total" -text "Valor"
        $nome.lista configure -yscrollcommand [list $nome.scwv set] -xscrollcommand [list $nome.scwh set]
        $nome.scwv configure -command [list $nome.lista yview]
        $nome.scwh configure -command [list $nome.lista xview]
        [self] atualiza_lista
    }

    method novo {}\
    {
        set dlg [DLGParcelados new $nome]
        if [$dlg run]\
        {
            set valoresd [$dlg get]
            set sql_cmd "INSERT INTO parcelados(descricao,data,pparcela,nparcelas,valor) SELECT '[dict get $valoresd {descricao}]', \
            '[dict get $valoresd {data}]', \
            '[dict get $valoresd {primeiraparcela}]',\
            '[dict get $valoresd {numeroparcelas}]',\
            '[dict get $valoresd {valor}]' WHERE NOT EXISTS (SELECT 1 FROM parcelados WHERE descricao='[dict get $valoresd {descricao}]' \
            AND valor='[dict get $valoresd {valor}]' AND nparcelas='[dict get $valoresd {numeroparcelas}]')"
            db eval $sql_cmd
           [self] atualiza_lista
        }
        $dlg destroy
    }

    method editar {}\
    {
        set selecionado [$nome.lista item [$nome.lista selection] -values]
        if {[llength $selecionado]<=0 || [lindex $selecionado 0]==""} return
        set dlg [DLGParcelados new  $nome]
        $dlg set [dict create "descricao" [lindex $selecionado 1]\
        "data" [lindex $selecionado 2] \
        "primeiraparcela" [lindex $selecionado 3] \
        "numeroparcelas" [lindex $selecionado 4] \
        "valor" [lindex $selecionado 8]]
        if [$dlg run]\
        {
            set valores [$dlg get]
            set sql_cmd "UPDATE parcelados SET descricao='[dict get $valores {descricao}]',\
            data='[dict get $valores {data}]', \
            pparcela='[dict get $valores {primeiraparcela}]',\
            nparcelas='[dict get $valores {numeroparcelas}]',\
            valor='[dict get $valores {valor}]' WHERE codigo='[lindex $selecionado 0]'"
            db eval $sql_cmd
            [self] atualiza_lista
        }
        $dlg destroy
    }

    method apagar {}\
    {
        set selecionado [$nome.lista item [$nome.lista selection] -values]
        if {[llength $selecionado]<=0 || [lindex $selecionado 0]==""} return
        set str "Apagar o parcelamento: \n[lindex $selecionado 1] \n Quantidade parcelas: [lindex $selecionado  4]\n Data Contratação:[lindex $selecionado 2] \n Valor [lindex $selecionado 8]"
        if {[tk_messageBox -message $str -type yesno]==yes}\
        {
            db eval "DELETE FROM parcelados WHERE codigo='[lindex $selecionado 0]'"
            [self] atualiza_lista
        }
    }

    method atualiza_lista {}\
    {
        $nome.lista delete [$nome.lista children {}]
        set total_parcelas 0
        set total_saldo 0
        set total_valor 0
        db eval "SELECT codigo,descricao,data,pparcela,nparcelas,valor FROM parcelados ORDER BY descricao" {
            set d1 [ split  $pparcela "-"]
            set d2 [ split [clock format [clock seconds] -format "%Y-%m"] "-"]
            set numparcelaspagas [expr (([lindex $d2 0] - [lindex $d1 0])*12)+([string trimleft [lindex $d2 1]  0]- [string trimleft [lindex $d1 1] 0])]
            set vparcela [expr $valor/$nparcelas]
            if {$nparcelas<=$numparcelaspagas} \
            {
                set saldo 0
                set numparcelaspagas $nparcelas
            } else \
            {
                set saldo [expr $valor - ($numparcelaspagas * $vparcela)]
                set total_parcelas [expr $total_parcelas+$vparcela]
                set total_saldo [expr $total_saldo+$saldo]
                set total_valor [expr $total_valor + $valor]
            }
            $nome.lista insert "" end -values   [list $codigo $descricao $data $pparcela $nparcelas $numparcelaspagas [format "%.2f" $vparcela] [format "%.2f" $saldo] [format "%.2f" $valor]]
        }
        $nome.lista insert "" end -values [list "" "Totais" "" "" "" "" [format "%.2f" $total_parcelas] [format "%.2f" $total_saldo] [format "%.2f" $total_valor]]
    }

    destructor \
    {
        destroy $nome
    }
}

