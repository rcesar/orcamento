#
# Calculadora de expr
# quando pressiona enter na entry usa o comando expr 
# para calcular e mostra o resultado na fita.
package require base64

::oo::class create Calculadora {
	variable nome
	variable img_apagar
	variable img_enter
	variable img_fechar

	constructor {}\
	{
	set img_apagar [image create photo -data [base64::decode "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAkpJREFUeNp0kk+IElEcx39vFBc9+OfQRTAwzFt4CaYOKStj6MoeculStzoIQSB4kCVckmDx4iGCXWYJIqjoVOzO1l4qT1F7WfBWHvxzDPyTB3XUmXn93suRybUffHmP997n9/cRsFgwGARJkiAcDsPlwgEIeEZQAhCRAkgAlOD6SQP4rgMFDWVnYCAQgFgsBqFQCBwOByzZNQOotPHx1RNCCCipu6bfb+zSnslkeOQVILPrBkAirbws9btdTEWAzZPXpfepOzaeGMBXwe/3w3+MwTc3Dl+UeghTiskbBvR6Pbh18mZHB0jjmxvCKhIfR37s3r+Sevf8ca/T4TBF2HTSODuDxP7uNjrZFFbBk8lEzOVyspa4ykGYw2zfbTb/7ilvok1YhlVVFfP5vDydTkHXdXDdlhZOOnPY4/HA0YPtp3h6LFjh8XgsFgoFGTPgsKm1zDr8ajTQh8Fh5eGjZzjGI8yjKlgjF4tFGdd/YKYmRja24hw+zu3sYe2HiH3hYzQjl8tleTQanWtou93G6Qngdrth6+1+9h6hTULJZ/PeziJXKhV5OByeg1ut1gJOp9NZTdNOcQ419ot+ggp1qoLdBFmqVmNpm3A8Huewy+Wq1RH8QH9zmBlJJpMRdCIqiiIPBgN+2MCGsW/r8/kgGo1m0fmpzWarseayHlmNeL1eFiWC0cRqtSr3+/3FpSiKHMZtjU1glbFyfKgLTqfzEka9OJvNeDnzz1JnCaFmqOl8ZdJY1SiDOXCiXKg1NtG5DIt0y6ov3dE/AgwAENFWYYLj4mYAAAAASUVORK5CYII="]]
	set img_enter [image create photo -data [base64::decode "iVBORw0KGgoAAAANSUhEUgAAABYAAAAWCAYAAADEtGw7AAAABmJLR0QA4ADhAODZvSRPAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH6AwWBCgs7s28mAAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAE1SURBVDjLzZQxSgNBFIY/MwlGBImIzYCaPMGgjXbGRkHRwtYz5Aja5ABWXkDwHOIh1ErsppxGjJAUWjhg84pFZiebdQM+GHb2H963//4zu/Bfy4idj+m1CtgXswL3jdhGpWAjtgUcAGtVOz4B6kCnavCZXmVW4OkcG7GLibXtTLbFHRuxS8BVAbdRx/VE4zXQLAjeLOTYiO0B/cTbNIHDjNQyYpeTYG26Szy0BpwDC7+WOpOiGABdnXeN2AGwoaMNrAONSJ8AT1GwEbsLXGakno4i1Y5GYcQa4HbChqbqOy/jLWDnDx/LRxQcnH8FjoFhSfB79mYusut7wD2wqtKNNgVgpNoX8KnzscbwHJwf5oIVLsCDHqH94PzjtPajZzU474Aj4AVYKZNL7r8iOO+BU+CtDPgHZzkyjtWMknUAAAAASUVORK5CYII="]]
	set img_fechar [image create photo -data [base64::decode "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA4ADhAODZvSRPAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH6AwJAjUNUT/7oQAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAN7SURBVDjLJctLU5NXAIDh95wEYghgMAQbEwQhWNCAVB0cRmqrOFZHu+hMqysW/Ax/gbvuu2Jaxs7YdlEtHcdWWrVVIUq5SMM9ESMkqZHEXD6S73a68Nk/4s7cQwCqVZ3YYkwaujGg5UojfV2Hhxs99WEBlMva+vT8wuTK9tZ4oaxFr1z61I70HcFWCnFn9iHZnbeUCkVvOpG6ceajE6NHeiMuW0oMZWErqJGCWiQbK2vVnyZ+HUvs5K+3NNfnP//iMmJi8W+SL5NN2c3/bn116fJ5Z6NHGMoCFAKBQgAKhcIhJI6Kob797ub9WGr72uDpEznH6UvnZfpV+usvL1y5arikWF5ZRQjBnjo3CoESoJSiVCyxtrqGw+0WgydPdsReLHpnl5bvShQD/R09o659e8Xy6gab/loexZdJpzMo3udCscDvU1Ms+R08XXjBbq0Uw+fOjBq6OSAT68mRSG/EpZk6gdABUktx7A8DPEqskkpnyBeK3Hs6hdHbxnYyQ2dbK6ZtEewKu3yN3hFnyN8yLN01mKaOx9vIZ0ePM/E8Sl1fmLur/0Jew328i0ziNRf2txNoDWIBlkPS0tk+LH2N3nDZqGLYBoZt4vE2cLH3JLm5FWR3iEpvO+lX25z1tRI8GMJUNrptYmFh1daEnShFxTKpKgshBAKBdDlxFCoU3mnYQmLmitS3eTCxsS0bpRQIgSkFslQurTsdEhMbUynKmsajp1PIY2GKyTT6ykvqug/x80yUbDaLoWwM20QzDQr67rpcWFufrLUUVduiVC7z15NpCp0B3qTecNbfysWeY2ixOHSFuB2N8ja3g65MMlqZfHZnUjrr6saTqxvVGgTxtTi5YBOp7TSnGpo40B6kwd/EuZ4I+somekeA+cVlKpZNbDNRTczHxqVhGtEH08/G/JZT7Q9+gExsMbjXT6izDcO2MGyDhuYmzvZE8G2kOBg8wMt8Tv3zeGrMKJai4pvfbvP4ybOmsNd3KzTUf77eu1cIQCmweU8pBSgAXhfeqfuTD+7H/5y61vfJUM65z9cAVS1389701eb40o0jHw+O9oQPufxuD06hsJXCAnYqu8S3MtUX08/HtmYWrgc62vPd/UcRP8z8gUAwG53l+x9/kTVu90Drid6RxkDLcIN7T9ghFFpFXy9m306+nlsaN8patGfolH24P4Lb6eR/Oe7J/useZuQAAAAASUVORK5CYII="]]
	set nome [toplevel ".calculadora[clock clicks]"]
	wm title $nome "Calculadora"
	wm geometry $nome "300x400"
	pack [frame $nome.toolbar -relief ridge -border 1] -side bottom -fill x
	pack [button $nome.toolbar.b1  -command [list [self] destroy ] -image $img_fechar -height 32] -side left -fill x -expand 1
	pack [frame $nome.toolbar.sep -width 20 -border 4px -relief flat] -side left -fill y
	pack [button $nome.toolbar.b2  -command [list [self] limpar ] -image $img_apagar -height 32] -side left -fill x -expand 1
	pack [button $nome.toolbar.b3  -command [list [self] calcular ] -image $img_enter -height 32] -side left -fill x -expand 1
	pack [entry $nome.et] -side bottom -fill x -expand 1
	pack [scrollbar $nome.scv -orient vertical] -side right -fill y
	pack [scrollbar $nome.sch -orient horizontal] -side bottom -fill x
	pack [text $nome.dsp -background white -xscrollcommand [list $nome.sch set] -yscrollcommand [list $nome.scv set] -wrap none] -side top -fill both -expand 1
	$nome.scv configure -command [list $nome.dsp yview]
	$nome.sch configure -command [list $nome.dsp xview]
	bind $nome.dsp <KeyPress> break
	bind $nome.et <KeyPress> [list [self] ke %k]
	}

 method ke {ke}\
	{
		if {$ke=="36"} {[self] calcular}
	}

 method limpar {}\
	{
	$nome.dsp delete 1.0 end
	$nome.et delete 0 end
	}

 method calcular {}\
	{
	set r [expr [$nome.et get]]
	$nome.dsp insert end "[$nome.et get]= $r \n"
	}
 destructor {
	destroy $nome
	}
}
