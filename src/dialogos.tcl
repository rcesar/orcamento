#
#
#
#
#


oo::class create DLGEmprestimos {
	variable nome
	variable retorno

    constructor {parent} \
    {
		set retorno 0
		set nome [toplevel "$parent.dlgEmprestimos[clock clicks]"]
		wm title $nome "Emprestimo"
		set x [expr [winfo x "."] +10]
		set y [expr [winfo y "."] +10]
		wm geometry $nome "600x200+$x+$y"
		#linha 1
		pack [ttk::frame $nome.fr1] -side top -fill x
		pack [ttk::labelframe $nome.fr1.descricao -text "Descrição:"  -relief flat] -side left -fill both -expand 1
		pack [ttk::entry $nome.fr1.descricao.et] -side top -fill both -expand 1
		pack [ttk::labelframe $nome.fr1.data -text "Data contratação:"  -relief flat] -side right
		pack [iwidgets::dateentry $nome.fr1.data.et -int 1] -side top -fill both -expand 1
		# linha 2
		pack [ttk::frame $nome.fr2] -side top -fill x
		pack [ttk::labelframe $nome.fr2.datapparcela -text "Data primeira parcela:"  -relief flat] -side left
		pack [iwidgets::dateentry $nome.fr2.datapparcela.et -int 1] -side top -fill both -expand 1
		pack [ttk::labelframe $nome.fr2.quantidadeparcelas -text "Quantidade de parcelas:" -relief flat] -side left
		pack [ttk::spinbox $nome.fr2.quantidadeparcelas.et -from 1 -to 100] -side top -fill both -expand 1
		pack [ttk::labelframe $nome.fr2.valorparcela -text "Valor da parcela:"  -relief flat] -side left -fill x -expand 1
		pack [ttk::entry $nome.fr2.valorparcela.et] -side top -fill both -expand 1
		#linha 3
		pack [ttk::labelframe $nome.valor -relief flat -text "Valor do emprestimo:"] -side top -fill x -expand 1
		pack [ttk::entry $nome.valor.et] -side top -fill both -expand 1
		#botoes
		pack [ttk::frame $nome.btbox -relief ridge -border 2px] -side bottom -fill x
		pack [ttk::button $nome.btbox.bt1 -text "OK" -command [list [self] ok]] -side right
		pack [ttk::button $nome.btbox.bt2 -text "Cancelar" -command [list [self] cancel]] -side right
		wm protocol $nome WM_DELETE_WINDOW [list wm withdraw $nome]
		wm withdraw $nome
	}
	method get {}\
	{
		set retorno [dict create "descricao" [$nome.fr1.descricao.et get]\
								 "data" [$nome.fr1.data.et get]\
								 "pparcela" [$nome.fr2.datapparcela.et get]\
								 "quantidadeparcelas" [$nome.fr2.quantidadeparcelas.et get]\
								 "valorparcela" [$nome.fr2.valorparcela.et get]\
								 "valor" [$nome.valor.et get]]
		return $retorno
	}

	method set {valor}\
	{
		$nome.fr1.descricao.et delete 0 end
		$nome.fr2.valorparcela.et delete 0 end
		$nome.valor.et delete 0 end
		$nome.fr1.descricao.et insert end [dict get $valor "descricao"]
	    $nome.fr1.data.et show [dict get $valor "data"]
		$nome.fr2.datapparcela.et show [dict get $valor "pparcela"]
		$nome.fr2.quantidadeparcelas.et set [dict get $valor "quantidadeparcelas"]
		$nome.fr2.valorparcela.et insert end [dict get $valor "valorparcela"]
		$nome.valor.et insert end [dict get $valor "valor"]
	}

	method cancel {}\
	{
	  set retorno 0
	  wm withdraw $nome
	}
	method ok {}\
	{
	   set retorno 1
	   wm withdraw $nome
	}

	method run {}\
	{
		my variable nome
		my variable retorno
		wm deiconify $nome
		update
		grab set $nome
		while 1\
		{
			update
			if {![winfo viewable $nome]} {break}
		}
		grab release $nome
		return $retorno
	}

	destructor \
	{
		destroy $nome
	}
}
#----------------------------------------------------------
oo::class create DLGParcelados {
    variable nome
    variable retorno

    constructor {parent} \
    {
		set retorno 0
		set nome [toplevel "$parent.dlgContas[clock clicks]"]
		wm title $nome "Compra parcelada"
		pack [ttk::frame $nome.l1] -side top -fill x
		pack [ttk::labelframe $nome.l1.descricao -text "Descrição:"] -side left -fill both -expand 1
		pack [ttk::labelframe $nome.l1.data -text "Data:"] -side right -fill both -expand 1
		pack [ttk::entry $nome.l1.descricao.et] -side top -fill both -expand 1
		pack [iwidgets::dateentry $nome.l1.data.et -int 1] -side top -fill both -expand 1
		pack [ttk::frame $nome.l2] -side top -fill x
		pack [ttk::labelframe $nome.l2.pparcela -text "Data primeira parcela:"] -side left -fill x -expand 1
		pack [ttk::labelframe $nome.l2.valor -text "Valor:"] -side left -fill x -expand 1
		pack [ttk::labelframe $nome.l2.numparcelas -text "Numero parcelas:"] -side left -fill x -expand 1
		pack [ttk::entry $nome.l2.valor.et] -side top -fill both -expand 1
		pack [iwidgets::dateentry $nome.l2.pparcela.et -int 1] -side top -fill both -expand 1
		pack [ttk::spinbox $nome.l2.numparcelas.et -from 1 -to 100]  -side top -fill both 
		pack [ttk::frame $nome.tb] -side bottom -fill x
		pack [ttk::button $nome.tb.bt1 -command [list [self] ok] -text "Ok"] -side left -fill x -expand true
		pack [ttk::button $nome.tb.bt2 -command [list [self] cancel] -text "Cancelar"] -side left -fill x -expand true
		set x [expr [winfo x "."] +10]
		set y [expr [winfo y "."] +10]
		wm geometry $nome "600x150+$x+$y"
		wm protocol $nome WM_DELETE_WINDOW [list wm withdraw $nome]
		wm withdraw $nome
	}
	method get {}\
	{
		set retorno [dict create "descricao" [$nome.l1.descricao.et get] \
								 "data"  [$nome.l1.data.et get] \
								 "primeiraparcela" [$nome.l2.pparcela.et get ]\
								 "numeroparcelas" [$nome.l2.numparcelas.et get ] \
								 "valor" [$nome.l2.valor.et get]]
		return $retorno
	}
	method set {valor}\
	{
		$nome.l1.descricao.et delete 0 end
		$nome.l2.valor.et delete 0 end
		$nome.l1.descricao.et insert end [dict get $valor "descricao"]
		$nome.l1.data.et show [dict get $valor "data"]
		$nome.l2.pparcela.et show [dict get $valor "primeiraparcela"]
		$nome.l2.valor.et insert end [dict get $valor "valor"]
		$nome.l2.numparcelas.et set [dict get $valor "numeroparcelas"]
	}

	method cancel {}\
	{
	  set retorno 0
	  wm withdraw $nome
	}
	method ok {}\
	{
	   set retorno 1
	   wm withdraw $nome
	}

	method run {}\
	{
		my variable nome
		my variable retorno
		wm deiconify $nome
		update
		grab set $nome
		while 1\
		{
			update
			if {![winfo viewable $nome]} {break}
		}
		grab release $nome
		return $retorno
	}

	destructor \
	{
		destroy $nome
	}
}

oo::class create DLGContas {
    variable nome
    variable retorno
    variable pago

    constructor {parent}\
    {
	    set retorno 0
        set nome [toplevel "$parent.dlgContas[clock clicks]"]
	    wm title $nome "Cadastro de conta mensal"
	    wm geometry $nome "400x200"
        pack [ttk::labelframe $nome.frData -text "Data:" -relief flat ] -side top -fill x
        pack [ttk::labelframe $nome.frCategoria -text "Categoria" -relief flat ] -side top -fill x
        pack [ttk::labelframe $nome.frValor -text "Valor:" -relief flat ] -side top -fill x
        pack [ttk::label $nome.pago -text "Conta paga ( )"] -side top -fill x 
        pack [iwidgets::dateentry $nome.frData.et -int 1] -side top -fill both -expand 1
	    pack [ttk::entry $nome.frCategoria.et] -side top -fill both -expand 1
	    pack [ttk::entry $nome.frValor.et] -side top -fill both -expand 1
	    pack [ttk::frame $nome.tb -relief ridge -border 3px] -side bottom -fill x
	    pack [ttk::button $nome.tb.bt1 -command [list [self] ok] -text "Ok"] -side right
	    pack [ttk::button $nome.tb.bt2 -command [list [self] cancel] -text "Cancel"] -side right
        wm protocol  $nome WM_DELETE_WINDOW [list wm_withdraw $nome]
        wm withdraw $nome
	    bind $nome.pago <1> [list [self] select_pago ]
	    set pago 0
    }

	method select_pago {}\
	{
	  if {$pago!=1} \
	   {
		set pago 1
		$nome.pago configure -text "Conta paga (X)"
	   } else {
		  set pago 0
		 $nome.pago configure -text "Conta paga ( )"
	   }
	}

	method get {}\
	{
	  set retorno [dict create \
	  "data" [$nome.frData.et get]\
	  "categoria" [$nome.frCategoria.et get]\
	  "valor" [$nome.frValor.et get]\
	  "pago" $pago]
	  return $retorno
	}
	method set {valores}\
	{
		$nome.frCategoria.et delete 0 end
		$nome.frValor.et delete 0 end
		$nome.frData.et show [dict get $valores "data"]
		$nome.frCategoria.et insert end [dict get $valores "categoria"]
		$nome.frValor.et insert end [dict get $valores "valor"]
		puts [dict get $valores "pago"]
		if {[dict get $valores "pago"]==1}\
		{
			set pago 1
			$nome.pago configure -text "Conta paga (X)"
		}
	}
	method cancel {}\
	{
	  set retorno 0
	  wm withdraw $nome
	}
	method ok {}\
	{
	   set retorno 1
	   wm withdraw $nome
	}

	method run {}\
	{
		my variable nome
		my variable retorno
		wm deiconify $nome
		update
		grab set $nome
		while 1\
		{
			update
			if {![winfo viewable $nome]} {break}
		}
		grab release $nome
		return $retorno
	}

	destructor \
	{
		destroy $nome
	}
}
##--------------------------------
::oo::class create GastoDLG {
	variable name
	variable retorno 0

	constructor {path}\
	{
		my variable name
		#coloca o dialogo exatamente sobre a janela principal util no android
		set pos [split [wm geometry .] "+"]
		set geo "300x200+[lindex $pos 1]+[lindex $pos 2]"
		#---------
		toplevel "$path"
		wm title $path "Lançamento"
		wm geometry $path $geo
		ttk::labelframe $path.data -text "Data:" -relief flat
		ttk::labelframe $path.descricao -text "Descricao" -relief flat
		ttk::labelframe $path.valor -text "Valor:"  -relief flat
		iwidgets::dateentry $path.data.entry -int 1
		ttk::entry $path.descricao.entry
		ttk::entry $path.valor.entry
		ttk::frame $path.tb
		ttk::button $path.tb.btOk -text "OK" -command [list [self] "ok"]
		ttk::button $path.tb.btCancel -text "Cancelar" -command [list [self] "cancel"]
		pack $path.data -side top -fill x
		pack $path.descricao -side top -fill x
		pack $path.valor -side top -fill x
		pack $path.data.entry -side top -fill both -expand 1
		pack $path.descricao.entry -side top -fill both -expand 1
		pack $path.valor.entry -side top -fill both -expand 1
		pack $path.tb -side bottom -fill x
		pack $path.tb.btOk -side left -fill x -expand 1
		pack $path.tb.btCancel -side left -fill x -expand 1
		wm protocol $path WM_DELETE_WINDOW [list wm withdraw $path]
		wm withdraw $path
		set name $path
	}

	method ok {}\
	{
		my variable name
		my variable retorno
		set retorno 1
		wm withdraw $name
	}

	method cancel {}\
	{
		my variable name
		my variable retorno
		set retorno 0
		wm withdraw $name
	}
	method get {} \
	{
		my variable name
		set data [$name.data.entry get]
		set descricao [$name.descricao.entry get]
		set valor [$name.valor.entry get]
		return [list $data $descricao $valor]
	}

	method set { values }\
	{
		my variable name
		$name.descricao.entry delete 0 end
		$name.valor.entry delete 0 end
		$name.data.entry show [lindex $values 0]
		$name.descricao.entry insert end [lindex $values 1]
		$name.valor.entry insert end [lindex $values 2] 
	}

	method run {}\
	{
		my variable name
		my variable retorno
		wm deiconify $name
		update
		grab set $name
		while 1\
		{
			update
			if {![winfo viewable $name]} {break}
		}
		grab release $name
		return $retorno
	}
	destructor\
	{
		my variable name
		destroy $name
	}
}
#========================================================
oo::class create DLGCartao {
	variable nome
	variable retorno

    constructor {parent} \
    {
		set retorno 0
		set nome [toplevel "$parent.dlgEmprestimos[clock clicks]"]
		wm title $nome "Cartão de crédito"
		set x [expr [winfo x $parent] +10]
		set y [expr [winfo y $parent] +10]
		wm geometry $nome "300x200+$x+$y"
		#linha 1
		pack [ttk::labelframe $nome.descricao -text "Descrição:" -relief flat] -side top -fill x
		pack [ttk::entry $nome.descricao.et] -side top -fill both -expand 1
		pack [ttk::labelframe $nome.limite -text "Limite:" -relief flat] -side top -fill x
		pack [ttk::entry $nome.limite.et] -side top -fill both -expand 1
		pack [ttk::frame $nome.tb] -side bottom -fill x
		pack [ttk::button $nome.tb.bt1 -command [list [self] ok] -text "Ok"] -side right
		pack [ttk::button $nome.tb.bt2 -command [list [self] cancel] -text "Cancelar"] -side right
		wm protocol $nome WM_DELETE_WINDOW [list wm_withdraw $nome]
	}
	method get {}\
	{
		set retorno [dict create "descricao" [$nome.descricao.et get]\
								 "limite" [$nome.limite.et get]]
		return $retorno
	}

	method set {valor}\
	{
		$nome.descricao.et delete 0 end
		$nome.limite.et delete 0 end
		$nome.descricao.et insert end [dict get $valor "descricao"]
		$nome.limite.et insert end [dict get $valor "limite"]
	}

	method cancel {}\
	{
	  set retorno 0
	  wm withdraw $nome
	}
	method ok {}\
	{
	   set retorno 1
	   wm withdraw $nome
	}

	method run {}\
	{
		my variable nome
		my variable retorno
		wm deiconify $nome
		update
		grab set $nome
		while 1\
		{
			update
			if {![winfo viewable $nome]} {break}
		}
		grab release $nome
		return $retorno
	}

	destructor \
	{
		destroy $nome
	}
}
#----------------------------------------------------------
oo::class create InputBox {
	variable nome
	variable retorno
	constructor {msg}\
	{
		set retorno 0
		set nome [toplevel .iptbox[clock click]]
		wm title $nome "Orçamento"
		set x [expr [winfo x "."] +10]
		set y [expr [winfo y "."] +10]		
		wm geometry $nome "300x100+$x+$y"
		pack [ttk::label $nome.label -text $msg] -side top
		pack [ttk::entry $nome.et] -side top -fill x -expand 1
		pack [ttk::frame $nome.tb] -side bottom -fill x -expand 1
		pack [ttk::button $nome.tb.ok -command [list [self] ok] -text "Ok"] -side right
		pack [ttk::frame $nome.tb.sep -width 8 -relief flat] -side right 
		pack [ttk::button $nome.tb.cancel -command [list [self] cancel] -text "Cancel"] -side right
		wm protocol $nome WM_DELETE_WINDOW [list wm withdraw .iptbox]
	}

	method ok {}\
	{
		set retorno 1
		wm withdraw $nome
	}

	method cancel {}\
	{
		set retorno 0
		wm withdraw $nome
	}

	method get {}\
	{
		return [$nome.et get]
	}

	method run {}\
	{
		wm deiconify $nome
		update
		grab set $nome
		while 1\
		{
			update
			if {![winfo viewable $nome]} {break}
		}
		grab release $nome
		return $retorno
	}

	destructor \
	{
		destroy $nome
	}		
}

proc input_box {msg} \
{
	set dlg [InputBox new $msg]
	set ret ""
	if [$dlg run] \
	{
		set ret [$dlg get]  
	}
	$dlg destroy
	return $ret
}
