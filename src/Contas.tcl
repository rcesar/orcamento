#
#
#
#

::oo::class create Contas {
    variable nome
    constructor {}\
    {
        set nome ".desktop.frContas[clock clicks]"
        ttk::frame $nome
        .desktop add $nome -text "Contas mensais"
        .desktop select $nome
        global imagens
        pack [ttk::frame $nome.toolbar -relief ridge -border 3px] -side top -fill x
        pack [ttk::button $nome.toolbar.bt2 -image [dict get $imagens "fechar"] -command [list [self] destroy]] -side left
        pack [ttk::frame $nome.toolbar.sep1 -border 2px -width 2px -relief raised] -side left -fill y
        pack [ttk::button $nome.toolbar.bt1 -image [dict get $imagens "mais"] -command [list [self] novo]] -side left
        pack [ttk::button $nome.toolbar.refreshbt -image [dict get $imagens "atualizar_p"] -command [list [self] atualiza_lista]] -side right
        pack [iwidgets::dateentry $nome.toolbar.dataini -int 1] -side right
        pack [ttk::label $nome.toolbar.lb -text "Data inicial:"] -side right
        pack [ttk::frame $nome.fr] -side top -fill both -expand 1
        pack [ttk::scrollbar $nome.fr.scv -orient vertical] -side right -fill y
        pack [ttk::scrollbar $nome.fr.sch -orient horizontal] -side bottom -fill x
        pack [ttk::treeview $nome.fr.lista -show headings -columns [list categorias mes1 mes2 mes3 mes4 mes5 mes6 mes7 mes8 mes9 mes10 mes11 mes12]] -side top -fill both -expand 1
        $nome.fr.lista configure -yscrollcommand [list $nome.fr.scv set] -xscrollcommand [list $nome.fr.sch set]
        $nome.fr.scv configure -command  [list $nome.fr.lista yview]
        $nome.fr.sch configure -command  [list $nome.fr.lista xview]
        $nome.fr.lista heading categorias -text "Categorias"
        $nome.fr.lista column categorias -width 120
        $nome.fr.lista column mes1 -width 80
        $nome.fr.lista column mes2 -width 80
        $nome.fr.lista column mes3 -width 80
        $nome.fr.lista column mes4 -width 80
        $nome.fr.lista column mes5 -width 80
        $nome.fr.lista column mes6 -width 80
        $nome.fr.lista column mes7 -width 80
        $nome.fr.lista column mes8 -width 80
        $nome.fr.lista column mes9 -width 80
        $nome.fr.lista column mes10 -width 80
        $nome.fr.lista column mes11 -width 80
        $nome.fr.lista column mes12 -width 80
        [self] atualiza_lista
        bind $nome.fr.lista <Double-1> [list [self] editar %W %x %y]
    }

    method novo {}\
    {
        set dlg [DLGContas new $nome]
        if [$dlg run] \
        {
             try {
               set dlgdict [$dlg get]
               set data [dict get $dlgdict "data"]
               set categoria [dict get $dlgdict "categoria"]
               set valor [dict get $dlgdict "valor"]
               set pago [dict get $dlgdict "pago"]
               if {$categoria=="" || $valor==""} return
               #O comando insert só vai inserir dados se não houver a descricao para o mes
               set sql_cmd "INSERT INTO contas(data,categoria,valor,pago) SELECT '$data','$categoria','$valor','$pago' WHERE NOT EXISTS ( SELECT 1 FROM contas WHERE categoria LIKE '$categoria' AND data='$data')"
               db eval $sql_cmd
           } on error {msg} {
                   puts $sqm_cmd
                   tk_messageBox -message $msg -icon error}
        }
        $dlg destroy
        [self] atualiza_lista
    }

    method editar {w x y}\
    {
            set selecao [$nome.fr.lista selection]
            if {$selecao ne ""}\
            {
                set item [$nome.fr.lista item $selecao -values]
                set col_index [$nome.fr.lista identify column $x $y]
                if {$col_index == ""} return
                set categoria [lindex $item 0]
                set col [string range $col_index 1 end]
                if {$col==1} return
                if {$categoria=="Total:"} return
                set mes [$nome.fr.lista heading "mes[expr $col - 1 ]" -text]
                set data [split  $mes "/"]
                set datai "[lindex $data 1]\-[lindex $data 0]\-01"
                set dataf "[lindex $data 1]\-[lindex $data 0]\-[dias_mes [lindex $data 0] [lindex $data 1]]"
                set dados [db eval "SELECT codigo,categoria,valor,data,pago FROM contas WHERE categoria='$categoria' AND data>='$datai' AND data<='$dataf'"]
                if {[llength $dados]<=0} return
                set ret [tk_dialog ".dleditaapaga" "Orçamento - contas mensais" "Apagar o registro selecionado ou editar" questhead 1 "Apagar" "Editar" "Cancelar"]
                destroy ".dleditaapaga"
                switch $ret {
                        0 { \
                                if {[tk_messageBox -message "Apagar o registro:\n[lindex $dados 1]\nData:[lindex $dados 3] Valor:[lindex $dados 2]?" -type yesno]==yes} {
                                        db eval "DELETE FROM contas WHERE codigo='[lindex $dados 0]'"
                                }
                           }
                        1 {
                                set dlg [DLGContas new $nome]
                                $dlg set [dict create \
                                "data" [lindex $dados 3]\
                                "categoria" [lindex $dados 1]\
                                "pago" [lindex $dados 4]\
                                "valor" [lindex $dados 2]]
                                if [$dlg run] \
                                {
                                        set dlgdict [$dlg get]
                                        set data [dict get $dlgdict "data"]
                                        set categoria [dict get $dlgdict "categoria"]
                                        set valor [dict get $dlgdict "valor"]
                                        set pago [dict get $dlgdict "pago"]
                                        db eval "UPDATE contas SET data='$data',valor='$valor',categoria='$categoria',pago='$pago' WHERE codigo='[lindex $dados 0]'"
                                }
                                $dlg destroy
                        }
                       default {return}
                   }
                        [self] atualiza_lista
                }
    }

    method atualiza_lista {}\
    {
        my variable nome
        $nome.fr.lista delete [$nome.fr.lista children {}]
        set tsp [clock scan [$nome.toolbar.dataini get] -format "%Y-%m-%d"]
        set meses [dict create]
        set soma -6
        array set dados { }
        set somas [list "Total:"]
        list categorias
        db eval "SELECT categoria FROM contas GROUP BY categoria" { set dados($categoria) [list "$categoria"] }
        set linha [list]
        for {set i 1} {$i<=12} {incr i}\
          {
                $nome.fr.lista heading "mes$i" -text [clock format [clock add $tsp $soma months] -format "%m/%Y"]
                set datai "[clock format [clock add $tsp $soma months] -format "%Y-%m"]-01"
                set dataf [split "[clock format [clock add $tsp $soma months] -format "%Y-%m"]" "-"]
                set dataf "[lindex $dataf 0]\-[lindex $dataf 1]\-[dias_mes [lindex $dataf 1] [lindex $dataf 0]]"
                foreach categoria [array names dados] \
                {
                        db eval "SELECT COALESCE( (SELECT valor  FROM contas WHERE categoria='$categoria' AND data>='$datai' AND data<='$dataf'  LIMIT 1), 0.00) as valor,COALESCE((SELECT pago FROM contas WHERE categoria='$categoria' AND data>='$datai' AND data<='$dataf' LIMIT 1), 0) as pago" \
                        { 
                                if {$pago==1}\
                                {
                                        lappend dados($categoria) [format "%.2f-pg" $valor]
                                } else {lappend dados($categoria) [format "%.2f" $valor]}
                        }
                }
                db eval "SELECT COALESCE((SELECT sum(valor) FROM  contas WHERE data>='$datai' AND data<='$dataf'),0.00) as total" {lappend somas [format "%.2f" $total]}
                incr soma
          }
        foreach categoria [array names dados] {
                $nome.fr.lista insert "" end -values $dados($categoria)
        }
        $nome.fr.lista insert "" end -values $somas
   }

   destructor \
   {
           destroy $nome
   }
}
