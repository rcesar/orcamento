#---------------------------------------------------------
::oo::class create Gastos { 
	variable name
	constructor {} \
	{
		global imagens
		my variable name
		set path [ttk::frame ".desktop.frGastos[clock click]"]
		ttk::frame $path.fr
		ttk::scrollbar $path.fr.scv -orient vertical
		pack $path.fr.scv -side right -fill y
		ttk::treeview $path.fr.lista -show headings -columns [list "codigo" "data" "descricao" "valor"] -displaycolumns {data descricao valor}
		$path.fr.lista heading data -text "Data:"
		$path.fr.lista heading descricao -text "Descrição:"
		$path.fr.lista heading valor -text "Valor:"
		$path.fr.lista column data -width 100
		$path.fr.lista column valor -width 100
		$path.fr.lista configure -yscrollcommand [list $path.fr.scv set]
 		pack [ttk::frame $path.tb -relief ridge -border 2px] -side top -fill x
		ttk::button $path.tb.btNovo -command [list [self] novo] -image [dict get $imagens "mais_p"]
		ttk::button $path.tb.btEditar  -command [list [self] editar] -image [dict get $imagens "editar_p"]
		ttk::button $path.tb.btApagar -command  [list [self] apagar] -image [dict get $imagens "apagar_p"]
		ttk::button $path.tb.btFechar -command [list [self] destroy] -image [dict get $imagens "fechar_p"]       
		pack $path.fr -side top -fill both -expand 1
		pack $path.fr.lista -side top -fill both -expand 1
		pack $path.tb.btFechar -side left 
        pack [ttk::frame $path.tb.sep' -border 2px -width 2px -relief raised] -side left
        pack $path.tb.btNovo -side left 
		pack $path.tb.btEditar -side left
		pack $path.tb.btApagar -side left 
        set name $path
        .desktop add $path -text "Gastos"
		.desktop select $path
		[self] atualiza_lista
	}
	method novo {}\
	{
		GastoDLG create dlg ".dlg[clock click -milliseconds]"
		if {[dlg run]}\
		{
			set valores  [dlg get]
			if {[string length [string trim [lindex $valores 1]]]>0}\
			{
				try { 
					db eval "INSERT INTO gastos(data,descricao,valor) VALUES('[lindex $valores 0]','[lindex $valores 1]','[lindex $valores 2]')"
				} on error {msg} {tk_messageBox -message $msg -icon error}
				[self] atualiza_lista
			}
		}
		dlg destroy
	}
	method editar {}\
	{
		my variable name
		set item [ $name.fr.lista item [ $name.fr.lista focus ] -values ]
		if {[llength $item]<=0} return
		GastoDLG create dlg ".dlgEdit"
		dlg set [lrange $item 1 end]  
		if {[dlg run]}\
		{
			set valores [dlg get]
			try {
				db eval "UPDATE gastos SET data='[lindex $valores 0]',descricao='[lindex $valores 1]',valor='[lindex $valores 2]' WHERE codigo='[lindex $item 0]'"
			} on error {msg} {tk_messageBox -message $msg -icon error}
		}
		dlg destroy
		[self] atualiza_lista	
	}
	method apagar {}\
	{
		my variable name
		set item [ $name.fr.lista item [ $name.fr.lista focus ] -values ]
		if {[llength $item]<=0} return
		set op [tk_messageBox -message "Apagar o lançamento\n[lindex $item 1] : [lindex $item 2] Valor [lindex $item 3]" -type yesno -icon question]
		if {$op == yes}\
		{
			db eval "DELETE FROM gastos WHERE codigo='[lindex $item 0]'"
			[self] atualiza_lista
		}
	}
	method atualiza_lista {}\
	{
		my variable name
		$name.fr.lista delete [$name.fr.lista children {} ]
		try {
			set soma 0.00
			db eval "SELECT codigo,data,descricao,valor FROM gastos WHERE fechado='0' ORDER BY data" \
			{
				$name.fr.lista insert "" end -values [list $codigo $data $descricao [format "%.2f" $valor]]
				set soma [expr $soma + $valor]
			}
			$name.fr.lista insert "" end -values [list "" "" "Total" [format "%.2f" $soma]]
		} on error {msg} {tk_messageBox -message $msg -icon error}
	}
	
	destructor \
	{
		my variable name
		destroy $name
	}
}
#======================================
