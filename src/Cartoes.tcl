#
#
#
#


oo::class create Cartao {
    variable nome
    variable parent
    constructor {p}\
    {
        set parent $p
        global imagens
        set nome ".cadCartao[clock click]"
        toplevel $nome
        wm geometry $nome "300x500"
        wm title $nome "Cartões de crédito"
        pack [ttk::frame $nome.tb -relief ridge -border 2px] -side top -fill x
        pack [ttk::button $nome.tb.bt1 -command [list [self] destroy] -image [dict get $imagens "fechar_p"]] -side left
        pack [ttk::frame $nome.tb.sep1 -border 2px -relief raised -width 2px] -side left
        pack [ttk::button $nome.tb.bt2 -command [list [self] novo] -image [dict get $imagens "mais_p"]] -side left
        pack [ttk::button $nome.tb.bt3 -command [list [self] editar] -image [dict get $imagens "editar_p"]] -side left
        pack [ttk::frame $nome.tb.sep2 -border 2px -relief raised -width 2px] -side left
        pack [ttk::button $nome.tb.bt4 -command [list [self] apagar] -image [dict get $imagens "apagar_p"]] -side left
        pack [ttk::scrollbar $nome.scv -orient vertical] -side right -fill y
        pack [ttk::treeview $nome.lista -yscrollcommand [list $nome.scv set] -columns [list "codigo" "descricao" "limite"] -displaycolumns [list "descricao" "limite"] -show headings] -side top -fill both -expand 1
        $nome.lista heading "descricao" -text "Descrição"
        $nome.lista heading "limite" -text "Limite"
        $nome.lista column "limite" -width 100
        $nome.scv configure -command [list $nome.lista yview]
        [self] atualiza_lista
    }

    method novo {}\
    {
        set dlg [DLGCartao new $nome]
        if [$dlg run]\
        {
            set valores [$dlg get]
            db eval "INSERT INTO cartoes(descricao,limite) SELECT '[dict get $valores {descricao}]','[dict get $valores {limite}]' WHERE NOT EXISTS( SELECT 1 FROM cartoes WHERE descricao='[dict get $valores {descricao}]')"
            [self] atualiza_lista
        }
        $dlg destroy 
    }

    method apagar {}\
    {
        set valores [$nome.lista item [$nome.lista selection] -values]
        if {[llength $valores] <=0} return
        set msg "Deseja apagar os dados do cartão [lindex $valores 1]?"
        if {[tk_messageBox -message $msg -type yesno -icon question]=="yes"}\
        {
            db eval "DELETE FROM cartoes WHERE codigo='[lindex $valores 0]'"
            [self] atualiza_lista
        }
    }

    method editar {}\
    {
        set valores [$nome.lista item [$nome.lista selection] -values]
        if {[llength $valores] <=0} return
        set dlg [DLGCartao new $nome]
        $dlg set [dict create "descricao" [lindex $valores 1] "limite" [lindex $valores 2]]
        if [$dlg run]\
        {
            set valoresd [$dlg get]
            if  [db eval "SELECT COALESCE((SELECT 1 FROM cartoes WHERE descricao = '[dict get $valoresd {descricao}]' LIMIT 1), 0) AS resultado"] \
            {
                tk_messageBox -message "Alteração contraria regras de validação de dados,cancelando" -icon error
            } else \
            {
                db eval "UPDATE cartoes SET descricao='[dict get $valoresd {descricao}]',limite='[dict get $valoresd {limite}]' WHERE codigo='[lindex $valores 0]'"
                [self] atualiza_lista
            }
        }
        $dlg destroy 
    }
    
    method atualiza_lista {} \
    {
        $nome.lista delete [$nome.lista children {}]
        db eval "SELECT codigo,descricao,limite FROM cartoes" {
            $nome.lista insert "" end -values [list $codigo $descricao [format "%.2f" $limite]]
        }
        $parent atualiza_cartoes
    }
    
    destructor \
    {
        my variable nome
        destroy $nome
    }
}
#--------------------------------------------------
oo::class create Faturas {
    variable nome
    variable cartoes
    constructor {} \
    {
        global imagens
        set nome ".faturas[clock click]"
        ttk::frame $nome 
        pack [ttk::frame $nome.tb -relief ridge -border 2px] -side top -fill x
        pack [ttk::button $nome.tb.bt1 -command [list [self] destroy] -image [dict get $imagens "fechar_p"]] -side left
        pack [ttk::frame $nome.tb.sep1 -border 2px -relief raised -width 2px] -side left
        pack [ttk::button $nome.tb.bt2 -command [list Cartao new [self]] -image [dict get $imagens "cartao_p"]] -side left
        pack [ttk::label $nome.tb.lb1 -text "Cartão:"] -side left
        pack [ttk::combobox $nome.tb.cartoes] -side left
        #Deixa a combobox readonly mas parecendo uma normal
        bind $nome.tb.cartoes <KeyPress> break 
        pack [ttk::button $nome.tb.btrf -command [list [self] abre_fatura] -image [dict get $imagens "abrir_p"]] -side left
        pack [ttk::notebook $nome.desktop] -side top -fill both -expand 1
        .desktop add $nome -text "Faturas de cartão de crédito"
        .desktop select $nome
        [self] atualiza_cartoes
    }
    method atualiza_cartoes {}\
    {
        set valores [list]
        set cartoes [dict create]
        db eval "SELECT codigo,descricao FROM cartoes GROUP BY descricao" \
        {
            lappend valores $descricao
            dict append cartoes $descricao $codigo 
        }
        $nome.tb.cartoes configure -values $valores
    }
    method abre_fatura {}\
    {
        my variable nome
        set codigo ""
        try  {
           set codigo [dict get $cartoes [$nome.tb.cartoes get]]
       } on error {msg} { }
       if {$codigo!=""} \
       {
            dsp_faturas new $nome $codigo
        }
    }
    


    
    destructor \
    {
        destroy $nome
    }
}

oo::class create dsp_faturas {
    variable nome
    variable codigo
    constructor {parent cod}\
    {
        global imagens
        set codigo $cod
        set tmp  [db eval "SELECT descricao,limite FROM cartoes WHERE codigo='$codigo' LIMIT 1"]
        set descricao [lindex $tmp 0]
        set limite [lindex $tmp 1]
        set nome "$parent.fatura[clock clicks]"
        ttk::frame $nome 
        pack [ttk::frame $nome.tb] -side top -fill x
        pack [ttk::button $nome.tb.bt1 -command [list [self] destroy ] -image [dict get $imagens "fechar_p"]] -side left
        pack [ttk::label $nome.tb.sep1 -text "|"] -side left
        pack [ttk::button $nome.tb.bt2 -command [list [self] add] -image [dict get $imagens "mais_p"]] -side left
        pack [ttk::button $nome.tb.bt3 -command [list [self] editar] -image [dict get $imagens "editar_p"]] -side left
        pack [ttk::label $nome.tb.sep3 -text "|"] -side left
        pack [ttk::button $nome.tb.bt5 -command [list [self] apagar] -image [dict get $imagens "apagar_p"]] -side left
        pack [ttk::label $nome.tb.sep2 -text "|"] -side left -fill y 
        pack [ttk::button $nome.tb.ffat -command [list [self] fechar_fatura] -image [dict get $imagens "fechar_fatura_p"]] -side  left        
        pack [ttk::frame $nome.fr1] -side top -fill x
        pack [ttk::labelframe $nome.fr1.descricao -text "Cartão:" -relief flat] -side left -expand true -fill x
        pack [ttk::entry $nome.fr1.descricao.et] -side top -fill both -expand 1
        bind $nome.fr1.descricao.et <KeyPress> break
        pack [ttk::labelframe $nome.fr1.limite -text "Limite:" -relief flat] -side left  -fill x
        pack [ttk::entry $nome.fr1.limite.et] -side top -fill both -expand 1
        bind $nome.fr1.limite.et <KeyPress> break
        pack [ttk::labelframe $nome.fr1.limite_disponivel -text "Limite disponivel:" -relief flat] -side right -fill x
        pack [ttk::entry $nome.fr1.limite_disponivel.et] -side top -fill both -expand 1
        bind $nome.fr1.limite_disponivel.et <KeyPress> break
        pack [ttk::scrollbar $nome.scv -orient vertical -command [list $nome.lista yvew] ] -side right -fill y
        pack [ttk::treeview $nome.lista -columns [list "codigo" "data" "descricao" "valor"] -displaycolumns [list "data" "descricao" "valor"] -show headings] -side top -fill both -expand 1
        $nome.fr1.descricao.et insert end $descricao
        $nome.fr1.limite.et insert end [format "%.2f" $limite]
        $nome.lista heading "data" -text "Data"
        $nome.lista heading "descricao" -text "Descrição"
        $nome.lista heading "valor" -text "valor"
        [self] atualiza_lista
        $parent.desktop add $nome -text "Fatura cartão: $descricao"
        $parent.desktop select $nome
    }
    
    method apagar {}\
    {
        set item [$nome.lista item [$nome.lista selection] -values]
        if {[lindex $item]<=0} return
        set msg "Apagar o lançamento \n [lindex $item 1] - [lindex $item 2] Valor [lindex $item 3]\n do Cartão: [$nome.fr1.descricao.et get]"
        if {[tk_messageBox -message $msg -type yesno -icon question]=="yes"}\
        {
            db eval "DELETE FROM faturas WHERE codigo='[lindex $item 0]'"
        }
        [self] atualiza_lista
    }
    
    method editar {}\
    {
        set item [$nome.lista item [$nome.lista selection] -values]
        if {[lindex $item]<=0} return
        set dlg [GastoDLG new ".dlgCartaoGastosEditar"]
        $dlg set [list [lindex $item 1] [lindex $item 2] [lindex $item 3]]
        if [$dlg run] \
        {
            set ret [$dlg get]
            db eval "UPDATE faturas SET data='[lindex $ret 0]',descricao='[lindex $ret 1]',valor='[lindex $ret 2]' WHERE codigo='[lindex $item 0]'"
            [self] atualiza_lista
        }
        $dlg destroy
    }
    
    method add {} \
    {
        set dlg [GastoDLG new ".dlgCartaoGastos[clock click]"]
        if [$dlg run]\
        {
            set valores [$dlg get]
            db eval "INSERT INTO faturas(codigo_cartao,data,descricao,valor) SELECT \
            '$codigo','[lindex $valores 0]','[lindex $valores 1]','[lindex $valores 2]' WHERE NOT EXISTS\
            (SELECT 1 FROM faturas WHERE codigo_cartao='$codigo' AND descricao='[lindex $valores 1]' AND data='[lindex $valores 0]' AND valor='[lindex $valores 1]')"
        }
        $dlg destroy
        [self] atualiza_lista
    }

    method fechar_fatura {} \
    {
       if {[tk_messageBox -message "Fechar a fatura corrente do cartão [$nome.fr1.descricao.et get]?" -type yesno -icon question]=="yes"}\
        {
               db eval "UPDATE faturas SET fechado='1' WHERE codigo_cartao='$codigo'"
        }
        [self] atualiza_lista
    }

    method atualiza_lista {}\
    {
        $nome.lista delete [$nome.lista children {}]
        $nome.fr1.limite_disponivel.et delete 0 end
        $nome.fr1.limite_disponivel.et insert end [format "%.2f" [$nome.fr1.limite.et get]]
        set total 0
        db eval "SELECT codigo as cd,data,descricao,valor FROM faturas WHERE codigo_cartao='$codigo' AND fechado='0' ORDER BY data" \
        {
            set saldo [expr [ $nome.fr1.limite_disponivel.et get ] - $valor]
            $nome.fr1.limite_disponivel.et delete 0 end
            $nome.fr1.limite_disponivel.et insert end [format "%.2f" $saldo]
            $nome.lista insert "" end -values [ list $cd $data $descricao $valor]
            set total [expr $total + $valor]
        }
        $nome.lista insert "" end -values [list "" "" "Total" [format "%.2f" $total]]
    }
    
    destructor \
    {
        destroy $nome
    }
}
