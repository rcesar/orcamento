#
#Funcoe de apoio
#
#
#


#------------------------
#Calcula o numero de dias no mes/ultimo dia do mes a partir do mes e do ano
proc dias_mes {mes ano}\
{
    try {
        set datai [clock scan "01/$mes/$ano" -format "%d/%m/%Y"]
        set dataf [clock add [clock add $datai +1 month] -1 day]
    } on error {msg} {puts $msg}
    return [clock format $dataf -format "%d"]
}



