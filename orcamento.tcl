############
#
#
#
#
package require sqlite3
package require Img
package require Iwidgets

set appPath  [file normalize [file dirname $::argv0]]
#-----------------------
sqlite3 db [file join $appPath "db" "orcamento.db"]
source [file join $appPath "src" "tabelas.tcl"]
#-----------------------
set imagens [dict create \
    "sair"   [image create photo -file [file join $appPath "imagens" "sair.png"]] \
    "calculadora" [image create photo -file [file join $appPath "imagens" "calculadora.png"]]\
    "contas" [image create photo -file [file join $appPath "imagens" "contas.png"]]\
    "mais"   [image create photo -file [file join $appPath "imagens" "mais.png"]]\
    "fechar"   [image create photo -file [file join $appPath "imagens" "fechar.png"]]\
    "fechar_p"   [image create photo -file [file join $appPath "imagens" "fechar_p.png"]]\
    "atualizar_p" [image create photo -file [file join $appPath "imagens" "refresh_p.png"]]\
    "mais_p" [image create photo -file [file join $appPath "imagens" "mais_p.png"]]\
    "apagar" [image create photo -file [file join $appPath "imagens" "apagar.png"]]\
    "apagar_p" [image create photo -file [file join $appPath "imagens" "apagar_p.png"]]\
    "editar_p" [image create photo -file [file join $appPath "imagens" "editar_p.png"]]\
    "gastos" [image create photo -file [file join $appPath "imagens" "gastos.png"]]\
    "parcelados" [image create photo -file [file join $appPath "imagens" "parcelados.png"]]\
    "fundo" [image create photo -file [file join $appPath "imagens" "fundo.png"]]\
    "emprestimos" [image create photo -file [file join $appPath "imagens" "emprestimos.png"]]\
    "cartao" [image create photo -file [file join $appPath "imagens" "cartao.png"]]\
    "cartao_p" [image create photo -file [file join $appPath "imagens" "cartao_p.png"]]\
    "abrir" [image create photo -file [file join $appPath "imagens" "abrir.png"] -width 32 -height 32]\
    "abrir_p" [image create photo -file [file join $appPath "imagens" "abrir_p.png"]]\
    "fechar_fatura_p" [image create photo -file [file  join $appPath "imagens" "fechar_fatura_p.png"]]
]


source [file join $appPath "src" "mafin.tcl"]
source [file join $appPath "src" "calculadora.tcl"]
source [file join $appPath "src" "util.tcl"]
source [file join $appPath "src" "dialogos.tcl"]
source [file join $appPath "src" "Parcelados.tcl"]
source [file join $appPath "src" "Contas.tcl"]
source [file join $appPath "src" "Gastos.tcl"]
source [file join $appPath "src" "Emprestimos.tcl"]
source [file join $appPath "src" "Cartoes.tcl"]
source [file join $appPath "src" "Eventos.tcl"]
source [file join $appPath "src" "mainWindow.tcl"]

MainWindow new
